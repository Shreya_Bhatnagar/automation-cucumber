$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "line": 1,
  "name": "User logins to the solvup Application",
  "description": "",
  "id": "user-logins-to-the-solvup-application",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 2,
  "name": "User logins with valid credentials",
  "description": "",
  "id": "user-logins-to-the-solvup-application;user-logins-with-valid-credentials",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "user navigates to Solvup Test Url",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "User enters valid Email",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "User enters valid password",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "User clicks on the Login",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "User logins successful to Solvup Admin View",
  "keyword": "Then "
});
formatter.match({
  "location": "login.user_navigates_to_Solvup_Test_Url()"
});
formatter.result({
  "duration": 9351062956,
  "status": "passed"
});
formatter.match({
  "location": "login.user_enters_valid_Email()"
});
formatter.result({
  "duration": 57576139,
  "status": "passed"
});
formatter.match({
  "location": "login.user_enters_valid_password()"
});
formatter.result({
  "duration": 16894587,
  "status": "passed"
});
formatter.match({
  "location": "login.user_clicks_on_the_Login()"
});
formatter.result({
  "duration": 5248726897,
  "status": "passed"
});
formatter.match({
  "location": "login.user_logins_successful_to_Solvup_Admin_View()"
});
formatter.result({
  "duration": 5165799,
  "status": "passed"
});
formatter.uri("switchUser.feature");
formatter.feature({
  "line": 1,
  "name": "Switch to Myer Store",
  "description": "",
  "id": "switch-to-myer-store",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 2,
  "name": "Switch to any Myer Store for case creation",
  "description": "",
  "id": "switch-to-myer-store;switch-to-any-myer-store-for-case-creation",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "user clicks on the actions tab",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "user clicks on the switch users tab",
  "keyword": "And "
});
formatter.step({
  "line": 5,
  "name": "user enters \"store_name\" under switch my store",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "user clicks on Switch Button",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Login Status popup should display",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "USer click on Continue Button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "case creation page should display",
  "keyword": "Then "
});
formatter.match({
  "location": "login.user_clicks_on_the_actions_tab()"
});
formatter.result({
  "duration": 7172788152,
  "error_message": "org.openqa.selenium.NoSuchElementException: Unable to locate element: #second-nav-panel \u003e li:nth-child(3) \u003e a:nth-child(1)\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.6.0\u0027, revision: \u00276fbf3ec767\u0027, time: \u00272017-09-27T15:28:36.4Z\u0027\nSystem info: host: \u0027DESKTOP-FTUQ96J\u0027, ip: \u0027192.168.81.58\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_171\u0027\nDriver info: org.openqa.selenium.firefox.FirefoxDriver\nCapabilities [{moz:profile\u003dC:\\Users\\Shreya\\AppData\\Local\\Temp\\rust_mozprofile.kkhWVwJUYakh, rotatable\u003dfalse, timeouts\u003d{implicit\u003d0, pageLoad\u003d300000, script\u003d30000}, pageLoadStrategy\u003dnormal, moz:headless\u003dfalse, platform\u003dXP, moz:accessibilityChecks\u003dfalse, moz:useNonSpecCompliantPointerOrigin\u003dfalse, acceptInsecureCerts\u003dtrue, browserVersion\u003d61.0.1, platformVersion\u003d10.0, moz:processID\u003d15664, browserName\u003dfirefox, javascriptEnabled\u003dtrue, platformName\u003dXP, moz:webdriverClick\u003dtrue}]\nSession ID: 3e1eb62e-fcab-4845-b819-5116b099e5c6\n*** Element info: {Using\u003dcss selector, value\u003d#second-nav-panel \u003e li:nth-child(3) \u003e a:nth-child(1)}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:185)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:120)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:586)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:356)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByCssSelector(RemoteWebDriver.java:450)\r\n\tat org.openqa.selenium.By$ByCssSelector.findElement(By.java:430)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:348)\r\n\tat CucumberFramework.steps.login.user_clicks_on_the_actions_tab(login.java:59)\r\n\tat ✽.Given user clicks on the actions tab(switchUser.feature:3)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "login.user_clicks_on_the_switch_users_tab()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "store_name",
      "offset": 13
    }
  ],
  "location": "login.user_enters_under_switch_my_store(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "login.user_clicks_on_Switch_Button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "login.login_Status_popup_should_display()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "login.user_click_on_Continue_Button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "login.case_creation_page_should_display()"
});
formatter.result({
  "status": "skipped"
});
});