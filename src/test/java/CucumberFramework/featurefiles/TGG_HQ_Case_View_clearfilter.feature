Feature: Test Clear Filter on TGG HQ case view page
Scenario: Login with TGG HQ and click on clear filter on the case view page
Given User has login with valid admin credentials
And User switch to TGG Hq View
And User enter case id in the search box
When User clicks on the clear filter link
Then Search box should reset to blank