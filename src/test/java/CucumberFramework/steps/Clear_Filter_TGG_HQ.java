package CucumberFramework.steps;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Clear_Filter_TGG_HQ {

	WebDriver driver = new ChromeDriver();
	JavascriptExecutor js = (JavascriptExecutor) driver;
	
	@Given("^User has login with valid admin credentials$")
	public void user_has_login_with_valid_admin_credentials() throws Throwable {
		driver.manage().window().maximize();
        driver.get("https://staging-apps.solvup.com/login");
        System.out.println(driver.getTitle()); 
		
          driver.findElement(By.xpath("//*[@id=\"UserIdentifier\"]")).sendKeys("vivek.srivastava@ticgroup.com.au");
          driver.findElement(By.xpath("//*[@id=\"UserPassword\"]")).sendKeys("pass");
          driver.findElement(By.xpath("//*[@id=\"UserLoginForm\"]/button")).click();
          /*verifying the page title*/
          String pagetitle = driver.getTitle();
          String expectedtitle = "Solvup: troubleshooting, returns and repairs";
          if (pagetitle.equalsIgnoreCase(expectedtitle))
          {
              System.out.println("Login Successful");
          }
          else
          {
              System.out.println("Login Failed");
          }
  }
	    
	

	@Given("^User switch to TGG Hq View$")
	public void user_switch_to_TGG_Hq_View() throws Throwable {
	  
		 driver.findElement(By.className("active")).click();
	        driver.findElement(By.linkText("Switch users")).click();
	        js.executeScript("window.scrollBy(0,500)");
	        driver.findElement(By.xpath("//*[@id=\"UserId\"]")).sendKeys("5495");
	        driver.findElement(By.cssSelector("input.btn.btn-default")).click();
	        driver.findElement(By.xpath("//*[@id=\"disclaimer_modal\"]")).click();
	    }
	

	@Given("^User enter case id in the search box$")
	public void user_enter_case_id_in_the_search_box() throws Throwable {
	   
		driver.findElement(By.xpath("//*[@id=\"RequestSearch\"]")).sendKeys("3291314");
	}

	@When("^User clicks on the clear filter link$")
	public void user_clicks_on_the_clear_filter_link() throws Throwable {
		driver.findElement(By.cssSelector("a.link")).click();
    
	}

	@Then("^Search box should reset to blank$")
	public void search_box_should_reset_to_blank() throws Throwable {
		 WebElement inputBox = driver.findElement(By.xpath("//*[@id=\"RequestSearch\"]"));
	        String textInsideInputBox = inputBox.getAttribute("value");

	        // Check whether input field is blank
	        if(textInsideInputBox.isEmpty())
	        {
	           System.out.println("filter cleared");
	           System.out.println("Test pass");
	        }
	        else
	        {
	            System.out.println("Filters not cleared");
	            System.out.println("Test Failed");
	}

	}
	
}
